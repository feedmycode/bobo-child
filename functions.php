<?php
/**
 * LSX Starter Child functions
 *
 * @package bobo-child
 */

/**
 * Sets up theme defaults
 *
 * @package bobo-child
 * @subpackage setup
 */
function lsx_sct_child_setup() {
	load_child_theme_textdomain( 'bobo-child', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'lsx_sct_child_setup', 11 );

/**
 * Enqueues the parent and the child theme styles.
 *
 * @package bobo-child
 * @subpackage setup
 */
function lsx_sct_child_scripts() {
	// Fonts from LSX Theme. Add these lines if your website will use a different font.
	//wp_dequeue_style( 'lsx-header-font' );
	//wp_dequeue_style( 'lsx-body-font' );
	//wp_dequeue_style( 'lsx_font_scheme' );
	// Google Fonts. Add these lines if your website will use a different font.
	//wp_enqueue_style( 'bobo-child-quattrocento-sans', 'https://fonts.googleapis.com/css?family=Quattrocento+Sans:400,400i,700,700i' );

	wp_enqueue_script( 'bobo-child-scripts', get_stylesheet_directory_uri() . '/assets/js/custom.min.js', array( 'jquery' ) );
}
add_action( 'wp_enqueue_scripts', 'lsx_sct_child_scripts', 11 );

// Editor color palette.
function bobo_child_child_setup() {
	add_theme_support( 'editor-color-palette', array(
		array(
			'name' => esc_html__( 'Orange', 'bobo-child' ),
			'slug' => 'orange',
			'color' => '#eda700',
		),
		array(
			'name' => esc_html__( 'Dark Orange', 'bobo-child' ),
			'slug' => 'dark-orange',
			'color' => '#f7941d',
		),
		array(
			'name' => esc_html__( 'Dark gray', 'bobo-child' ),
			'slug' => 'dark-gray',
			'color' => '#222222',
		),
		array(
			'name' => esc_html__( 'Light Gray', 'bobo-child' ),
			'slug' => 'light-gray',
			'color' => '#f6f6f6',
		),
		array(
			'name' => esc_html__( 'Blue', 'bobo-child' ),
			'slug' => 'blue',
			'color' => '#013343',
		),
		array(
			'name' => esc_html__( 'Dark Blue', 'bobo-child' ),
			'slug' => 'dark-blue',
			'color' => '#01213a',
		),
		array(
			'name' => esc_html__( 'White', 'bobo-child' ),
			'slug' => 'white',
			'color' => '#ffffff',
		),
		array(
			'name' => esc_html__( 'Black', 'bobo-child' ),
			'slug' => 'black',
			'color' => '#000000',
		),
	));
}
add_action( 'after_setup_theme', 'bobo_child_child_setup', 100 );

/** Custom Login Screen **/
function bobo_child_custom_login() { ?>
	<style type="text/css">
		#login h1 a, .login h1 a {
			background-image:url(https://bobo.feedmybeta.com/wp-content/uploads/2019/04/logo.svg);
			height: 150px;
			width: auto;
			background-size: 278px 111px;
			background-repeat: no-repeat;
		}
		.login {
			background-color: #013343;
		} /* add url to background you want to use */
		.wp-core-ui .button-primary { /* change button colors */
			color: #fff;
			background: #013343 !important;
			border-color: #013343 !important;			
			text-decoration: none;
			text-shadow:none!important;
			box-shadow:none!important;
		}
		.login #backtoblog a, .login #nav a {
			color: #fff !important;
		}
	</style>
	<?php
}
add_action( 'login_enqueue_scripts', 'bobo_child_custom_login' );

/* Fix Logo URL */
function bobo_child_custom_login_url() {
	return home_url();
}
add_filter( 'login_headerurl', 'bobo_child_custom_login_url' );